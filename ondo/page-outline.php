<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody">
		<?php get_sidebar(); ?>

		<section class="mainArea">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post();
			
			// 社是
			$txt_date = nl2br(get_post_meta($post->ID, 'txt_main', true));
			$image_id = SCF::get('img_title'); 
			$image = wp_get_attachment_image_src($image_id, 'full');
			if (!empty($image_id) or !empty($txt_date)) {
				echo '
				<h2 class="headTitle02"><span class="fo24">社是</span></h2>
				<div class="grtArea">';
			if (!empty($image_id)) {
					echo '<h3 class="ttl_img"><img src="'.$image[0].'" alt=""></h3>';
			};
			if (!empty($txt_date)) {
					echo '<p class="txt1">'.$txt_date.'</p>';
			};
			echo '</div>';
			};
			
			
			// 会社概要
			$repeat_group = scf::get('field_company');
			$company_cnt = 0;
			foreach ( $repeat_group as $field_name => $field_value ) :
			$company_cnt++;
				if($field_value === reset($repeat_group)){
					echo '
					<h2 class="headTitle02"><span class="fo24">会社概要</span></h2>
					<div class="outlineArea">
					<table>';
				}
				$company_name = $field_value['company_name'];
				$company_detail = $field_value['company_detail'];
				echo '<tr>';
				echo '<th>'.$company_name.'</th>';
				echo '<td>'.nl2br($company_detail).'</td>';
				echo '</tr>';
				if ($field_value === end($repeat_group)) {
					echo '</table>';// 最後
					echo '</div>';
				}
			 endforeach;
			
			
			// 沿革
			$repeat_group = scf::get('field_history');
			$company_cnt = 0;
			foreach ( $repeat_group as $field_name => $field_value ) :
			$company_cnt++;
				if($field_value === reset($repeat_group)){
					echo '
					<h2 class="headTitle02"><span class="fo24">沿革</span></h2>
					<div class="hisArea">
					<table>';
				}
				$history_name = $field_value['history_name'];
				$history_detail = $field_value['history_detail'];
				echo '<tr>';
				echo '<th>'.$history_name.'</th>';
				echo '<td>'.nl2br($history_detail).'</td>';
				echo '</tr>';
				if ($field_value === end($repeat_group)) {
					echo '</table>';// 最後
					echo '</div>';
				}
			 endforeach;
			?>
			<?php endwhile; endif; ?>
		</section>
	</div>
</div><!-- //#content -->

<?php get_footer(); ?>
