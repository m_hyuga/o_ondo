<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div id="contents">

	<div id="mainBody">
		<?php get_sidebar(); ?>

		<section class="mainArea">
			<p>申し訳ありません。<br>お探しのページは見つかりませんでした。</p>
		</section>
	</div>
</div><!-- //#content -->

<?php get_footer(); ?>
