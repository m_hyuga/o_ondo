<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="description" content="石川県金沢市で、ユニットハウス・プレハブハウスのレンタル、建築工事一式、足場工事の３事業を柱に、快適な住空間の創造に取り組んでいます。">
<meta name="keywords" content="音頭金属、プレハブ、ユニットハウス、オンキン、">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css">
	<?php if (is_home()): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css" />
	<?php endif; if (is_page('outline') || is_parent_slug() === 'outline' ): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/outline.css" />
	<?php endif; if (is_page('product') ): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/product.css" />
	<?php endif; if (is_parent_slug() === 'product' ): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/product_detail.css" />
	<?php endif; if (is_page('recruit')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/recruit.css" />
	<?php endif; if (is_page('form')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/form.css" />
	<?php endif; if (is_page('sitemap')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sitemap.css" />
	<?php endif; if (is_post_type_archive('works') || is_singular('works') || is_tax('works_cat')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/works.css" />
	<?php endif; if (is_post_type_archive('news') || is_singular('news') || is_tax('news_cat') || is_search() ): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/editor-style.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/news.css" />
	<?php endif; ?>

<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/scrolltopcontrol.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.tile.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/common/js/html5shiv.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/css3-mediaqueries.js"></script>
<![endif]-->
<?php if (is_home()): ?>
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery.flexslider.js"></script>
<script src="<?php bloginfo('template_url'); ?>/common/js/jquery-bgswitcher/jquery.bgswitcher.js"></script>
<script>
$(window).load(function(){
	$(".aboutArea").bgswitcher({
		images: ["<?php bloginfo('template_url'); ?>/images/top/section_about01.jpg","<?php bloginfo('template_url'); ?>/images/top/section_about02.jpg","<?php bloginfo('template_url'); ?>/images/top/section_about03.jpg","<?php bloginfo('template_url'); ?>/images/top/section_about04.jpg"]
	});
});
</script>
<script>
$(document).ready(function() {
	$('.flexslider').flexslider({
        animation: "fade",
		slideshowSpeed: 4000
      });
});
</script>
<?php endif; ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-73070840-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
</script>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">

<header id="headerSp">
<div class="inner clearfix">
	<p class="logo"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/sp_logo.gif" alt="音頭金属株式會社"></a></p>
	<p class="menuBtn open"><img src="<?php bloginfo('template_url'); ?>/common/images/sp_menu.gif" alt="menu"></p>
	<div id="menu">
		<span class="bgMenu close">&nbsp;</span>
		<div class="menuBg">
			<div class="menuHead">
				<p class="logo"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/sp_logo_on.png" alt="音頭金属株式會社"></a></p>
				<p class="closeBtn close"><img src="<?php bloginfo('template_url'); ?>/common/images/close.gif" alt="close"></p>
			</div>
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
				<li class="child"><a href="<?php bloginfo('url'); ?>/outline/">会社概要</a></li>
				<li class="child"><a href="<?php bloginfo('url'); ?>/outline/greeting/">　ー 代表挨拶</a></li>
				<li class="child"><a href="<?php bloginfo('url'); ?>/outline/office/">　ー 事業所一覧</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/group/">　ー グループ一覧</a></li>
				<li><a href="<?php bloginfo('url'); ?>/product/">製品案内</a></li>
				<li><a href="<?php bloginfo('url'); ?>/works/">施工事例</a></li>
				<li><a href="<?php bloginfo('url'); ?>/news/">新着情報</a></li>
				<li><a href="<?php bloginfo('url'); ?>/recruit/">採用情報</a></li>
				<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ</a></li>
			</ul>
		</div>
	</div>
</div>
</header><!-- //#headerSp -->

<?php if (is_home()): ?>
	<div id="slideArea">
		<div class="flexslider">
			<ul class="slides">
				<li><p><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/top/mainimg01.jpg" alt="Variation 確かな技術と多くの実績で地域をもっと便利にする"></a></p></li>
				<li><p><a href="<?php bloginfo('url'); ?>/product/"><img src="<?php bloginfo('template_url'); ?>/images/top/mainimg02.jpg" alt="Unithouse ユニット建築　Preafabricated construction プレハブ建築"></a></p></li>
				<li><p><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/top/mainimg03.jpg" alt="Preafabricated construction オンキンハウス プレハブ建築　詳しくはこちら"></a></p></li>
				<li><p><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/top/mainimg04.jpg" alt="Yess construction 詳しくはこちら"></a></p></li>
			</ul>
		</div>
	</div>
<?php endif; ?>


<header id="header">
<div class="hinner clearfix">
	<p id="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/logo.gif" alt="音頭金属株式會社"></a></p>
	<nav id="navi">
		<ul class="clearfix">
			<li><a href="<?php bloginfo('url'); ?>/outline/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn01.gif" alt="会社概要"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn02.gif" alt="製品案内"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/works/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn03.gif" alt="施工事例"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/news/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn04.gif" alt="新着情報"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/common/images/navi_btn05.gif" alt="採用情報"></a></li>
		</ul>
	</nav>
	<dl class="hbtn">
		<dt><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_contact.gif" alt="お問い合わせ"></a></dt>
		<dd><a href="javascript:void(0);"><img src="<?php bloginfo('template_url'); ?>/common/images/btn_search.gif" alt="search"></a></dd>
	</dl>
	<div id="searchBox">
		<span class="sankaku"></span>
		<?php
					// 検索フォームを出力する
					get_search_form();
					 
					// 検索フォームを代入する
					$search_form = get_search_form(false); ?>
	</div>
</div>
</header><!-- //#header -->