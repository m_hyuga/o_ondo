<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody">
		<?php get_sidebar(); ?>

		<section class="mainArea">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<div class="recruitArea">
					<h2 class="headTitle02"><span class="fo24">新卒採用</span></h2>
					<?php
					$check_shinsotsu = nl2br(get_post_meta($post->ID, 'check_shinsotsu', true));
					if($check_shinsotsu == '表示する'){ ?>
					<h3>募集要項</h3>
					<table>
					<?php //新卒　要項
						$field_shokushu = nl2br(get_post_meta($post->ID, 'field_shokushu', true));
						$field_shikaku = nl2br(get_post_meta($post->ID, 'field_shikaku', true));
						$field_gakka = nl2br(get_post_meta($post->ID, 'field_gakka', true));
						$field_saiyoukou = nl2br(get_post_meta($post->ID, 'field_saiyoukou', true));
						$field_shorui = nl2br(get_post_meta($post->ID, 'field_shorui', true));
						$field_houhou = nl2br(get_post_meta($post->ID, 'field_houhou', true));

						if (!empty($field_shokushu)) {
								echo '<tr><th>募集職種</th><td>'.$field_shokushu.'</td></tr>';
						};
						if (!empty($field_shikaku)) {
								echo '<tr><th>応募資格</th><td>'.$field_shikaku.'</td></tr>';
						};
						if (!empty($field_gakka)) {
								echo '<tr><th>募集学部・学科</th><td>'.$field_gakka.'</td></tr>';
						};
						if (!empty($field_saiyoukou)) {
								echo '<tr><th>募集人数</th><td>'.$field_saiyoukou.'</td></tr>';
						};
						if (!empty($field_shorui)) {
								echo '<tr><th>応募書類</th><td>'.$field_shorui.'</td></tr>';
						};
						if (!empty($field_houhou)) {
								echo '<tr><th>選考方法</th><td>'.$field_houhou.'</td></tr>';
						};
					?>
					</table>
					<h3>待遇</h3>
					<table>
					<?php //新卒　待遇
						$sin_shoninkyu = nl2br(get_post_meta($post->ID, 'sin_shoninkyu', true));
						$sin_shoteate = nl2br(get_post_meta($post->ID, 'sin_shoteate', true));
						$sin_shokyu = nl2br(get_post_meta($post->ID, 'sin_shokyu', true));
						$sin_shoyo = nl2br(get_post_meta($post->ID, 'sin_shoyo', true));
						$sin_kyujitsu = nl2br(get_post_meta($post->ID, 'sin_kyujitsu', true));
						$sin_kinmuchi = nl2br(get_post_meta($post->ID, 'sin_kinmuchi', true));
						$sin_zikan = nl2br(get_post_meta($post->ID, 'sin_zikan', true));
						$sin_hoken = nl2br(get_post_meta($post->ID, 'sin_hoken', true));
						$sin_fukuri = nl2br(get_post_meta($post->ID, 'sin_fukuri', true));

						if (!empty($sin_shoninkyu)) {
								echo '<tr><th>初任給</th><td>'.$sin_shoninkyu.'</td></tr>';
						};
						if (!empty($sin_shoteate)) {
								echo '<tr><th>諸手当</th><td>'.$sin_shoteate.'</td></tr>';
						};
						if (!empty($sin_shokyu)) {
								echo '<tr><th>昇給</th><td>'.$sin_shokyu.'</td></tr>';
						};
						if (!empty($sin_shoyo)) {
								echo '<tr><th>賞与</th><td>'.$sin_shoyo.'</td></tr>';
						};
						if (!empty($sin_kyujitsu)) {
								echo '<tr><th>休日</th><td>'.$sin_kyujitsu.'</td></tr>';
						};
						if (!empty($sin_kinmuchi)) {
								echo '<tr><th>勤務地</th><td>'.$sin_kinmuchi.'</td></tr>';
						};
						if (!empty($sin_zikan)) {
								echo '<tr><th>勤務時間</th><td>'.$sin_zikan.'</td></tr>';
						};
						if (!empty($sin_hoken)) {
								echo '<tr><th>保険</th><td>'.$sin_hoken.'</td></tr>';
						};
						if (!empty($sin_fukuri)) {
								echo '<tr><th>福利厚生</th><td>'.$sin_fukuri.'</td></tr>';
						};
					?>
					</table>
					<h3>応募</h3>
					<table>
					<?php //新卒　応募
						$sin_oubo = nl2br(get_post_meta($post->ID, 'sin_oubo', true));
						$sin_event = nl2br(get_post_meta($post->ID, 'sin_event', true));

						if (!empty($sin_oubo)) {
								echo '<tr><th>応募方法</th><td>'.$sin_oubo.'</td></tr>';
						};
						if (!empty($sin_event)) {
								echo '<tr><th>イベント</th><td>'.$sin_event.'</td></tr>';
						};
						echo '</table>';
						
						
						// バナー
						$repeat_group = scf::get('field_sinlink');
						echo '<div class="bnr_mynavi cf">';
						foreach ( $repeat_group as $field_name => $field_value ) :
						$sin_url = $field_value['sin_url'];
						echo '<a href="'.$sin_url.'" target="_blank">';
						$val =  $field_value["sin_bunnar"];
							if (!empty($val)) {
							$image = wp_get_attachment_image_src($val, 'full');
							echo '<img src="'.$image[0].'">';
							}
							echo '</a>';
					 endforeach;
					 echo '</div>';
					?>
					</table>
					<?php
					} else {
						echo '<p class="recruit_end">今年度の募集は終了しました。</p>';
					}
					?>

					
					<h2 class="headTitle02"><span class="fo24">中途採用</span></h2>
					<?php
					$check_chuto = nl2br(get_post_meta($post->ID, 'check_chuto', true));
					if($check_chuto == '表示する'){ ?>
					<h3>募集要項</h3>
					<table>
					<?php //中途　要項
						$chu_shokushu = nl2br(get_post_meta($post->ID, 'chu_shokushu', true));
						$chu_shikaku = nl2br(get_post_meta($post->ID, 'chu_shikaku', true));
						$chu_shogu = nl2br(get_post_meta($post->ID, 'chu_shogu', true));
						$chu_kinmuchi = nl2br(get_post_meta($post->ID, 'chu_kinmuchi', true));
						$chu_zikan = nl2br(get_post_meta($post->ID, 'chu_zikan', true));
						$chu_holiday = nl2br(get_post_meta($post->ID, 'chu_holiday', true));
						$chu_hoken = nl2br(get_post_meta($post->ID, 'chu_hoken', true));
						$chu_fukuri = nl2br(get_post_meta($post->ID, 'chu_fukuri', true));
						$chu_shorui = nl2br(get_post_meta($post->ID, 'chu_shorui', true));
						$chu_houhou = nl2br(get_post_meta($post->ID, 'chu_houhou', true));

						if (!empty($chu_shokushu)) {
								echo '<tr><th>募集職種</th><td>'.$chu_shokushu.'</td></tr>';
						};
						if (!empty($chu_shikaku)) {
								echo '<tr><th>応募資格</th><td>'.$chu_shikaku.'</td></tr>';
						};
						if (!empty($chu_shogu)) {
								echo '<tr><th>処遇</th><td>'.$chu_shogu.'</td></tr>';
						};
						if (!empty($chu_kinmuchi)) {
								echo '<tr><th>勤務地</th><td>'.$chu_kinmuchi.'</td></tr>';
						};
						if (!empty($chu_zikan)) {
								echo '<tr><th>勤務時間</th><td>'.$chu_zikan.'</td></tr>';
						};
						if (!empty($chu_holiday)) {
								echo '<tr><th>休日</th><td>'.$chu_holiday.'</td></tr>';
						};
						if (!empty($chu_hoken)) {
								echo '<tr><th>保険</th><td>'.$chu_hoken.'</td></tr>';
						};
						if (!empty($chu_fukuri)) {
								echo '<tr><th>福利厚生</th><td>'.$chu_fukuri.'</td></tr>';
						};
						if (!empty($chu_shorui)) {
								echo '<tr><th>応募書類</th><td>'.$chu_shorui.'</td></tr>';
						};
						if (!empty($chu_houhou)) {
								echo '<tr><th>選考方法</th><td>'.$chu_houhou.'</td></tr>';
						};
					?>
					</table>
					<h3>応募</h3>
					<table>
					<?php //中途　応募
						$chu_oubo = nl2br(get_post_meta($post->ID, 'chu_oubo', true));

						if (!empty($chu_oubo)) {
								echo '<tr><th>応募方法</th><td>'.$chu_oubo.'</td></tr>';
						};
					?>
					</table>
					<?php
					} else {
						echo '<p class="recruit_end">今年度の募集は終了しました。</p>';
					}
					?>
				</div>
				<div class="contactArea">
					<h2 class="headTitle02"><span class="fo24">採用に関するお問い合わせ先</span></h2>
					<p>当社の事業内容をご覧いただき、ご興味をお持ちの場合には<span class="spnone"><br></span>まず、採用担当までお気軽にお問合せください。<br><br>詳細な募集要項、選考方法等はお問合せの際にお答え致します。</p>
					<div class="inq">
						<h4>音頭金属株式会社</h4>
						<p>&#12306;920-0811 石川県金沢市小坂町中18番地4</p>
						<p class="tel"><span>TEL.</span>076-252-6155</p>
					</div>
				</div>
				</div>
			<?php endwhile; endif; ?>

<?php get_footer(); ?>
