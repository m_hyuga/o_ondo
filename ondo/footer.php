<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<footer id="footer">
	<div class="mailArea">
		<p class="mailTit spon fo13">お問い合わせはこちら</p>
		<div class="mail">
			<p class="pcon"><img src="<?php bloginfo('template_url'); ?>/common/images/mail_img.jpg" alt="お問い合わせはこちら ご質問やご相談など、お気軽にお問い合わせください。 076-252-6155 （平日 00:00～00:00）"></p>
			<p class="spon tel"><a href="tel:076-252-6155"><img src="<?php bloginfo('template_url'); ?>/common/images/top_sp_tel.gif" alt="076-252-6155 （平日 00:00～00:00）"></a></p>
			<p class="btn"><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/mail_btn.jpg" alt="お問い合わせフォームはこちら" class="pcon"><img src="<?php bloginfo('template_url'); ?>/common/images/sp_mail_btn.gif" alt="お問い合わせフォームはこちら" class="spon"></a></p>
		</div>
	</div>
<p class="pageTop spon"><a href="#wrapper"><img src="<?php bloginfo('template_url'); ?>/common/images/sp_page_top.gif" alt="Pagetop"></a></p>
<div id="finner">
<nav id="fnaviSp">
	<ul>
				<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/">会社概要</a></li>
				<li><a href="<?php bloginfo('url'); ?>/product/">製品案内</a></li>
				<li><a href="<?php bloginfo('url'); ?>/works/">施工事例</a></li>
				<li><a href="<?php bloginfo('url'); ?>/news/">新着情報</a></li>
				<li><a href="<?php bloginfo('url'); ?>/recruit/">採用情報</a></li>
				<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ</a></li>
	</ul>
</nav>
<div class="flogo clearfix">
	<p><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/flogo.gif" alt="音頭金属株式會社"></a></p>
	<address class="fo14">〒920-0811　石川県金沢市小坂町中18-4<span class="pcon">TEL.076-252-6155 ／ FAX.076-252-9761</span><span class="spon">TEL　076-252-6155 &#12857;<span class="pl20">FAX　076-252-9761</span></span></address>
</div>
<div class="fnavi clearfix">
	<ul class="flinks">
		<li><a href="<?php bloginfo('url'); ?>/outline/">会社概要</a>
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/outline/greeting/">代表挨拶</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/group/">グループ一覧</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/office/">事業所一覧</a></li>
			</ul>
		</li>
	</ul>
	<ul class="flinks widDif">
		<li class="clearfix"><a href="<?php bloginfo('url'); ?>/product/">製品案内</a>
				<?php wp_nav_menu( array('theme_location'=>'footer_menu1'));	?>
				<?php wp_nav_menu( array('theme_location'=>'footer_menu2'));	?>
		</li>
	</ul>
	<ul class="flinks widDif02">
		<li><a href="<?php bloginfo('url'); ?>/works/">施工事例</a>
				<?php wp_nav_menu( array('theme_location'=>'side_works'));	?>
		</li>
	</ul>
	<ul class="flinks pr00">
		<li><a href="<?php bloginfo('url'); ?>/news/">新着情報</a></li>
		<li><a href="<?php bloginfo('url'); ?>/recruit/">採用情報</a></li>
		<li><a href="<?php bloginfo('url'); ?>/form/">お問い合わせ</a></li>
		<li><a href="<?php bloginfo('url'); ?>/sitemap/">サイトマップ</a></li>
	</ul>
</div>
</div>
<p class="copyright fo10"><small>Copyright(c) 2015 OndoKinzoku Corporation. All Rights Reserved</small></p>
</footer>
</div><!-- #wrapper -->
<?php wp_footer(); ?>

</body>
</html>
