<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="contents">
	<section class="mainimg">
		<div class="inner">
			<h1 class="ttl"><img src="../images/outline/ttl.png" alt="会社概要　outline" class="pcon"><img src="../images/outline/sp_mainimg.jpg" alt="会社概要　outline" class="spon"></h1>
		</div>
	</section>

	<div id="pnavi">
		<ul class="fo14 clearfix">
			<li><a href="../index.html">HOME</a>&nbsp;／&nbsp;</li>
			<li>会社概要</li>
		</ul>
	</div>

	<div id="mainBody">
		<?php get_sidebar(); ?>

		<section class="mainArea">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</section>
	</div>
</div><!-- //#content -->

<?php get_footer(); ?>
