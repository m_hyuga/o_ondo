<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody">
		<?php get_sidebar(); ?>

	<section class="mainArea">
		<div class="productDetail">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post();

			//メイン画像
			$image_id = SCF::get('img_main');
			$image = wp_get_attachment_image_src($image_id, 'full');
			if (!empty($image_id)) {
					echo '<p class="mimg"><img src="'.$image[0].'"></p>';
			};

			$txt_mains = nl2br(get_post_meta($post->ID, 'txt_mains', true));
			if (!empty($txt_mains)) {
				echo '<p class="mitx">'.$txt_mains.'</p>';
			}

				echo '<div class="pointArea">';
			//ポイント
			$point_list = array('1','2','3','4','5','6','7');
			foreach ($point_list as $point_num){
				$ttl_point = nl2br(get_post_meta($post->ID, 'ttl_point0'.+$point_num, true));
				$txt_point = nl2br(get_post_meta($post->ID, 'txt_point0'.+$point_num, true));
				$image_id = SCF::get('img_point0'.+$point_num);
				$image = wp_get_attachment_image_src($image_id, 'full');
				$link_point = nl2br(get_post_meta($post->ID, 'link_point0'.+$point_num, true));
				$mds_frow = nl2br(get_post_meta($post->ID, 'mds_frow0'.+$point_num, true));

				if (!empty($ttl_point) or !empty($txt_point)) {
					if (!empty($ttl_point)) {
						echo '<dl class="title"><dt><img src="'.get_bloginfo('template_url').'/images/product/unithouse/point0'.$point_num.'.gif"></dt><dd class="fo18 spL">'.$ttl_point.'</dd></dl>';
					}
					if (!empty($txt_point)) {
						echo '<div class="point_txtimg clearfix">';
						if (!empty($image_id)) {
								echo '<div><a href="';
						if (!empty($link_point)) { echo $link_point.'" target="_brank">';}else{echo $image[0].'" class="colorbox">';};
						echo '<img src="'.$image[0].'" width="100%" alt=""></a></div>';
						};
						echo '<p class="txt fo14">'.$txt_point.'</p>';
						echo '</div>';
					}

					$field_num = 'field_frow0'.+$point_num;
					$repeat_group = scf::get($field_num);
					$field_cnt = 0;
					$frow_list = '';
					$frow_num_result = '';
					foreach ( $repeat_group as $field_name => $field_value ) :
						$ttl_frow = $field_value['ttl_frow0'.+$point_num];
						$frowimage_id = SCF::get('img_frow0'.+$point_num);
						$img_frow =  $field_value['img_frow0'.+$point_num];
						if (!$ttl_frow == '' or !$img_frow == '') {
							$frow_list .= '<li>';
							if (!empty($img_frow)) {
								$image = wp_get_attachment_image_src($img_frow, 'full');
								$frow_list .= '<span><a href="'.$image[0].'" class="colorbox"><img src="'.$image[0].'" alt=""></a></span>';
							}
							if (!empty($ttl_frow)) {
								$frow_list .= '<p class="caption">'.$ttl_frow.'</p>';
							}
							$frow_list .= '</li>';

							$field_cnt ++;
							//echo $field_cnt;
							if ($field_value === end($repeat_group)) {
								if($field_cnt <= 3) {$frow_num_result = 'flow_03';}
								echo '<div class="'.$frow_num_result.' flow_box">';
								if (!empty($mds_frow)) { echo '<h4>'.$mds_frow.'</h4>';}
								echo '<ul class="clearfix">';
								echo $frow_list;
								echo '</ul>';
								echo '</div>';
							}
						}
					endforeach;
				}
			}
			echo '</div>';




			//注釈
			$repeat_group = scf::get('field_chushaku');
			foreach ( $repeat_group as $field_name => $field_value ) :
				$ttl_chushaku = $field_value['ttl_chushaku'];
				$txt_chushaku = $field_value['txt_chushaku'];
				$img_chushaku =  $field_value['img_chushaku'];

				if (!empty($ttls_chushaku) or !empty($txt_chushaku)) {

					if ($field_value === reset($repeat_group)) {
						echo '<div class="optimumArea">';
						echo '<p class="title fo18">'.get_the_title().'はこのような方に最適です<span><img src="'.get_bloginfo('template_url').'/images/product/unithouse/ico.png" ></span></p>';
					}
					echo '<div class="cont clearfix">';
					if (!empty($img_chushaku)) {
						$image = wp_get_attachment_image_src($img_chushaku, 'full');
						echo '<p class="img"><img src="'.$image[0].'" alt=""></p>';
					}
					echo '<div>';
					if (!empty($ttl_chushaku)) {
						echo '<p class="tit fo16">'.$ttl_chushaku.'</p>';
					}
					if (!empty($txt_chushaku)) {
						echo '<p class="txt fo14">'.$txt_chushaku.'</p>';
					}
					echo '</div>';
					echo '</div>';
					if ($field_value === end($repeat_group)) { echo '</div>';}

				}
			endforeach;


			//注意点
			$repeat_group = scf::get('field_caution');
			foreach ( $repeat_group as $field_name => $field_value ) :
				$ttl_caution = $field_value['ttl_caution'];
				$txt_caution = $field_value['txt_caution'];

				if (!empty($ttl_caution) or !empty($txt_caution)) {

					if ($field_value === reset($repeat_group)) { echo '<div class="attentionArea"><p class="title fo18">ご利用の注意点</p>';}
					echo '<div class="cont">';
					if (!empty($ttl_caution)) {
						echo '<p class="tit fo16">'.$ttl_caution.'</p>';
					}
					if (!empty($txt_caution)) {
						echo '<p class="txt fo14">'.$txt_caution.'</p>';
					}
					echo '</div>';
					if ($field_value === end($repeat_group)) { echo '</div>';}

				}
			endforeach;

			echo '</div>';



			//製品一覧
			if(is_page('system')){
				$page_type = 'system';
			}
			if(is_page('prefab-lease')){
				$page_type = 'prefab-lease';
			}
			if(is_page('prefab')){
				$page_type = 'prefab';
			}
			if(is_page('unithouse-lease')){
				$page_type = 'unithouse-lease';
			}
			if(is_page('unithouse')){
				$page_type = 'unithouse';
			}
			if(is_page('ippan')){
				$page_type = 'ippan';
			}
			if(is_page('ashiba')){
				$page_type = 'ashiba';
			}
			if(is_page('machine-lease')){
				$page_type = 'machine-lease';
			}
			if(is_page('doboku')){
				$page_type = 'doboku';
			}


			$echo_group = 0;
			query_posts(
				array(
				'post_type' => 'product_detail',
				'posts_per_page' => -1,
				'tax_query' => array(
            array(
                'taxonomy' => 'product_detail_cat',
                'field' => 'slug',
                'terms' => $page_type,
                ),
            ),
				 )
			);
			if (have_posts()) : while (have_posts()) : the_post();
			if (is_first_post()) {echo '<h2><img src="'.get_bloginfo('template_url').'/images/product/unithouse/ttl.gif" alt="製品一覧" class="pcon"><img src="'.get_bloginfo('template_url').'/images/product/unithouse/sp_ttl.gif" alt="製品一覧" class="spon"></h2><div class="productList">';}


			$echo_ttl_list = '' ;
			$echo_ttl_list .= '<p class="fo18">'.get_the_title().'</p>';
			$terms = get_the_terms( get_the_ID(), 'product_detail_tag' );
			if ( !empty($terms) ) {
				$term_cnt = 0;
				if ( !is_wp_error( $terms ) ) {
					foreach( $terms as $term ) {
						if ($term === reset($terms)) { $echo_ttl_list .= '<ul>';}
						$echo_ttl_list .= '<li>'.$term->name.'</li>';
						$term_cnt ++;
						if ($term === end($terms)) { $echo_ttl_list .= '</ul>';}
					}
				}
			}
			echo '<div class="title';
			if($term_cnt >= 1) { echo '';}
			echo '">';
			echo $echo_ttl_list;
			echo '</div>';
			echo '<div class="product clearfix"><div class="clearfix">';

			$echo_group++;
			$repeat_group = scf::get('field_product');
			echo '<script type="text/javascript">
						$(function(){
						$("a[rel=\'a_group0'.$echo_group.'\']").colorbox();
						$("a[rel=\'b_group0'.$echo_group.'\']").colorbox();
						});
						</script>';
			$echo_slides = '';

			// 配列の最初にポインタを移動
			reset($repeat_group);
			// このときのキーを取得
			list($first_key) = each($repeat_group);

			// 配列の最後にポインタを移動
			end($repeat_group);
			// このときのキーを取得
			list($last_key) = each($repeat_group);
			foreach ( $repeat_group as $field_name => $field_value ) :
			$val =  $field_value["img_product"];
				if (!empty($val)) {
				$image = wp_get_attachment_image_src($val, 'full');
				if ($field_name === $first_key){
					$echo_slides .= '<p><a href="'.$image[0].'" class="colorbox" rel="a_group0'.$echo_group.'"><img src="'.$image[0].'"></a></p>
					<ul>';
				} else {
				$echo_slides .= '<li><a href="'.$image[0].'" class="colorbox" rel="a_group0'.$echo_group.'"><img src="'.$image[0].'"></a></li>';
				}
				if ($field_name === $last_key){
					$echo_slides .= '</ul>';
				}
				}
			 endforeach;
			echo '<div class="photos">';
			echo $echo_slides;
			echo '</div>';
			echo '<div>';
			$txt_product = nl2br(get_post_meta($post->ID, 'txt_product', true));
			if (!empty($txt_product)) {
				echo '<p class="txt fo14">'.$txt_product.'</p>';
			}
			echo '<ul class="btn">';
			$file_id = SCF::get('file_product');
			$file = wp_get_attachment_url($file_id);
			if (!empty($file_id)) {
			echo '<li><a href="'.$file.'" target="_blank"><img src="'.get_bloginfo('template_url').'/images/product/unithouse/btn02.gif" alt="図面PDF(0,000KB)" class="pcon"><img src="'.get_bloginfo('template_url').'/images/product/unithouse/sp_btn02.gif" alt="図面PDF(0,000KB)" class="spon"></a></li>';
			};
			echo '</ul>';
			echo '</div></div>';
			$repeat_group = scf::get('field_product_detail');
			foreach ( $repeat_group as $field_name => $field_value ) :
				$detail_title = $field_value['detail_title'];
				$detail_txt = $field_value['detail_txt'];
				if (!empty($detail_title) or !empty($detail_txt)) {
					if ($field_value === reset($repeat_group)) { echo '<table class="product_table">';}
					echo '<tr>';
					echo '<th>'.nl2br($detail_title).'</th>';
					echo '<td>'.nl2br($detail_txt).'</td>';
					echo '</tr>';
					if ($field_value === end($repeat_group)) { echo '</table>';}
				}
			endforeach;

			echo '</div>';


			endwhile; endif; wp_reset_query();




			// yessのみ
			if(is_page('system')) : ?>
			<div class="bnr_area clearfix">
				<h2>yess建築についての詳細は下記メーカーサイトまで</h2>
				<ul>
					<li>横河システム建築ホームページ<br><a href="http://www.yokogawa-yess.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/product/bnr_system1.jpg"></a></li>
					<li>yessビルダーズNET HPリンク<br><a href="http://www.builder-net.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/product/bnr_system2.jpg"></a></li>
				</ul>
			</div>
		<?php endif;


			endwhile; endif; ?>


			<?php

			echo '<h2><img src="'.get_bloginfo('template_url').'/images/product/unithouse/ttl02.gif" alt="実績紹介" class="pcon"><img src="'.get_bloginfo('template_url').'/images/product/unithouse/sp_ttl02.gif" alt="実績紹介" class="spon"></h2>';
			echo '<div class="actualArea">
				<ul class="workList clearfix">';
			query_posts(
				array(
				'post_type' => 'works',
				'posts_per_page' => 6,
				'tax_query' => array(
            array(
                'taxonomy' => 'works_cat',
                'field' => 'slug',
                'terms' => $page_type,
                ),
            ),
				 )
			);
			if (have_posts()) : while (have_posts()) : the_post();
				$image_id = SCF::get('img_thumb');
				$image = wp_get_attachment_image_src($image_id, 'full');
				if (!empty($image_id)) {
					$img_thumb =  '<img src="'.$image[0].'">';
				};
				$txt_date =  scf::get('works_point');
				?>
			<li><a href="<?php echo get_the_permalink(); ?>"><?php echo $img_thumb; ?><p><span><?php the_title(); ?></span><?php echo $txt_date; ?></p></a></li>
			<?php endwhile; endif; wp_reset_query();

			echo '</ul>
				<p class="btn pcon"><a href="'.get_bloginfo('url').'/works/works_cat/'.$page_type.'/"><img src="'.get_bloginfo('template_url').'/images/product/unithouse/btn03.gif" alt="施工事例をもっと見る"></a></p>
			</div>';
			?>

</section>
</div>
</div><!-- //#content -->


<?php get_footer(); ?>
