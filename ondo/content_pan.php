<section class="mainimg">
	<div class="inner">
	<?php if (is_page('outline') || is_parent_slug() === 'outline' ): ?>
		<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/outline/ttl.png" alt="会社概要" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/outline/sp_mainimg.jpg" alt="会社概要" class="spon"></h1>
	<?php endif; if (is_page('product') || is_parent_slug() === 'product' ): ?>
		<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/product/ttl.png" alt="製品案内" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/product/sp_mainimg.jpg" alt="製品案内" class="spon"></h1>
	<?php endif; if (is_page('recruit')): ?>
		<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/recruit/ttl.png" alt="採用情報" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/recruit/sp_mainimg.jpg" alt="採用情報" class="spon"></h1>
	<?php endif; if (is_page('form')): ?>
		<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/form/ttl.png" alt="お問い合わせ" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/form/sp_mainimg.jpg" alt="お問い合わせ" class="spon"></h1>
	<?php endif; if (is_page('sitemap')): ?>
		<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/sitemap/ttl01.png" alt="サイトマップ" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/sitemap/sp_mainimg.jpg" alt="サイトマップ" class="spon"></h1>
	<?php endif; if (is_post_type_archive('news') || is_singular('news') || is_tax('news_cat')): ?>
			<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/news/ttl.png" alt="新着情報" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/news/sp_mainimg.jpg" alt="新着情報" class="spon"></h1>
	<?php endif; if (is_post_type_archive('works') || is_singular('works') || is_tax('works_cat')): ?>
			<h1 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/works/ttl01.png" alt="施工事例" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/works/sp_mainimg.jpg" alt="施工事例" class="spon"></h1>
	<?php endif; ?>
	</div>
</section>

<div id="pnavi">
	<ul class="fo14 clearfix">
		<li><a href="<?php bloginfo('url'); ?>">HOME</a>&nbsp;／&nbsp;</li>
		<?php
		if (is_page() && $post->post_parent) { //固定ページの子ページ
			$parent_url = get_page_link($post->post_parent);
			$parent_title = get_the_title($post->post_parent);
			echo '<li><a href="'.$parent_url.'">'.$parent_title.'</a>&nbsp;／&nbsp;</li>';
			echo '<li>'.get_the_title().'</li>';
		}elseif(is_page()) { //ただの固定ページ
			echo '<li>'.get_the_title().'</li>';
		}
		if (is_post_type_archive()){ //アーカイブページ
			$blog_label = esc_html(get_post_type_object(get_post_type())->label);
			echo '<li>'.$blog_label.'</li>';
		}
		if (is_tax()){ //アーカイブページ
			$parent_url = get_page_link($post->post_parent);
			$blog_tax = $wp_query->get_queried_object();
			if(is_tax('works_cat')) {
			echo '<li><a href="'.$site_url.'/works/">施工事例</a>&nbsp;／&nbsp;</li>';
			}
			if(is_tax('news_cat')) {
			echo '<li><a href="'.$site_url.'/news/">新着情報</a>&nbsp;／&nbsp;</li>';
			}
			echo '<li>'.$blog_tax->name.'</li>';
		}
		if (is_single()){ //末端ページ
			$site_url = get_bloginfo('url');
			$blog_name = esc_html(get_post_type_object(get_post_type())->name);
			$blog_label = esc_html(get_post_type_object(get_post_type())->label);
			echo '<li><a href="'.$site_url.'/'.$blog_name.'/">'.$blog_label.'</a>&nbsp;／&nbsp;</li>';
			echo '<li>'.get_the_title().'</li>';
		}
		?>
	</ul>
</div>
