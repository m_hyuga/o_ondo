<?php
get_header();
?>
<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody">
		<?php get_sidebar(); ?>
		<section class="mainArea">
		
		<?php if (is_post_type_archive('news') || is_singular('news') || is_tax('news_cat')): ?>
				<h2 class="headTitle02"><span class="fo24"><?php
				if(is_tax()){
					$get_archive_term = $term;
					$get_term = get_term_by("slug", $get_archive_term, 'news_cat');
					echo $get_term->name;
				}else{
				echo '新着情報';
				}; ?></span></h2>
				<div class="newsArea">
					<ul>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<li>
							<div class="arc_head cf"><span class="time"><?php the_time('Y.m.d'); ?></span>
							<?php $terms = get_the_terms( get_the_ID(), 'news_cat' );
																			if ( !empty($terms) ) {
																				$term_slug = '';
																				if ( !is_wp_error( $terms ) ) {
																					foreach( $terms as $term ) {
																						$term_slug = $term -> slug;
																						if ($term_slug == 'information'){ $terms_class = 'cate_bg02';}
																						if ($term_slug == 'recruit'){ $terms_class = 'cate_bg03';}
																						if ($term_slug == 'works'){ $terms_class = 'cate_bg01';}
																						echo '<span class="cate '.$terms_class.'">';
																						echo $term->name.'</span>';
																					}
																				}
																			} ?></div>
							<div class="cf">
								<h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<p><?php
										$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 150);
										$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
										$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
										echo $theContentForPreSingle.'...';
										?></p>
							</div>
						</li>
					<?php endwhile; endif; wp_reset_query();?>


					</ul>
				</div>
				<?php endif; if (is_post_type_archive('works') || is_singular('works') || is_tax('works_cat')): ?>

				<h2 class="headTitle02 noline"><span class="fo24">施工事例一覧</span></h2>
				<ul class="workList clearfix">
					<?php if (have_posts()) : 
						while (have_posts()) : the_post();
						$image_id = SCF::get('img_thumb');
						$image = wp_get_attachment_image_src($image_id, 'full');
						if (!empty($image_id)) {
							$img_thumb =  '<img src="'.$image[0].'">';
						};
						$txt_date =  scf::get('works_point');
						?>
					<li><a href="<?php echo get_the_permalink(); ?>"><?php echo $img_thumb; ?><p><span><?php the_title(); ?></span><?php echo $txt_date; ?></p></a></li>
					<?php endwhile; endif; wp_reset_query();?>
				</ul>


				<?php endif;?>

				<!-- ページャー -->
				<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</section>
	</div>
</div><!-- //#content -->

<?php get_footer(); ?>
