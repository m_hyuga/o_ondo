<?php 
/*
YARPP Template: Pages
Author: mitcho (Michael Yoshitaka Erlewine)
Description: A simple example YARPP template.
*/
?>
<?php if (have_posts()):?>
<h3 class="headTitle03 wALL fo18">関連する事例を見る</h3>
<ul class="workList clearfix">
	<?php while (have_posts()) : the_post(); ?>
	<?php 
						$image_id = SCF::get('img_thumb');
						$image = wp_get_attachment_image_src($image_id, 'full');
						if (!empty($image_id)) {
							$img_thumb =  '<img src="'.$image[0].'">';
						};
						$txt_date =  scf::get('works_point');
						?>
					<li><a href="<?php echo get_the_permalink(); ?>"><?php echo $img_thumb; ?><p><span><?php the_title(); ?></span><?php echo $txt_date; ?></p></a></li>
	<?php endwhile; ?>
</ul>
<?php else: ?>
<?php endif; ?>

</div><!-- blogpage_content end -->
