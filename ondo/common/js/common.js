﻿function smartRollover() {if(document.getElementsByTagName) {var images = document.getElementsByTagName("img");for(var i=0; i < images.length; i++) {
if(images[i].getAttribute("src").match("_off.")){images[i].onmouseover = function() {this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));}
images[i].onmouseout = function() {this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));}}}}}if(window.addEventListener) {window.addEventListener("load", smartRollover, false);}else if(window.attachEvent) {window.attachEvent("onload", smartRollover);}


//smoothscroll
$(function(){
	$("a[href^=#]").click(function() {
		var myHref= $(this).attr("href");
		var myPos = $(myHref).offset().top;
		$("html,body").animate({scrollTop : myPos}, 700);
		return false;
	});
});

//
if ((navigator.userAgent.indexOf('iPhone') > 0) || navigator.userAgent.indexOf('iPod') > 0 ||

navigator.userAgent.indexOf('Android') > 0) {
	    document.write('<meta name="viewport" content="width=320">');
	}else{
	    document.write('<meta name="viewport" content="width=1100">');
}

// smenu
$(function(){
$(".open").click(function(){
    $("#menu").fadeIn(250);
});
$(".close").click(function(){
    $("#menu").fadeOut(250);
});
});

//#menu
$(document).ready(function(){
	$(window).bind('load resize',function(){
		var wH = $(window).height();
		$('#menu').css('height',wH+'0');
	});
});


//------------------------------
//	以下追加処理
//------------------------------

//----- サーチボックスのフォーカス処理tile_two
$(document).ready(function(){
	$('#searchBox input[type="text"]')
	.blur(function(){
		if($(this).val()=='検索はこちら'||$(this).val()=='') { //フォーカスを外した際、val値が "検索はこちら"か空白の場合
			$(this).css('color', '#a9a9a9').val('検索はこちら'); //val値に "検索はこちら"を入れて、色をグレーにする
		} else { //それ以外の文字の場合
			$(this).css('color', '#000'); //文字は黒色
		}
	})
	.focus(function(){
		if($(this).val()=='検索はこちら') { //フォーカスした際、val値が "検索はこちら"の場合
			$(this).css('color', '#000').val(''); //色は黒にして、val値を消す
		}
	});
	//----- サーチボックスの開閉処理
	$(document).on('click', '.hbtn dd', function(){
		//検索ボタン押したら
		$(this).parent('.hbtn').parent('.hinner').children('#searchBox').stop(true,false).fadeIn('fast');
		// $('#searchBox').stop(true,false).fadeIn('fast'); //フェードイン
	});
	$(document).on('click', '#btn_search_close', function(){
		//ボックスのバツ押したら
		$(this).parent('form').parent('#searchBox').stop(true,false).fadeOut('fast');
		// $('#searchBox').stop(true,false).fadeOut('fast'); //フェードアウト
	});
});
//----- 高さ揃え
$(function(){
		//.tilesizeが存在する場合のみ以下実行
		$(window).on('load resize', function(){ //ロード時とリサイズ時に以下実行
			var w = $(window).width();
			if($('.tilesize').size()) {
					$('.tile_two01 li').tile(2);
					$('.tile_two02 li').tile(2);
			}
			if($('.listArea').size()) {
					if (w <= 665) {
					$('.listArea li a').tile(2);
					} else {
					$('.listArea li a').tile(3);
					}
			}
			if($('.workList').size()){
					var listheight = $('.workList li a').height();
					$('.workList li a').height(listheight);
			}
		});
	//------ フッター固定処理
	$(window).on('scroll', function(){
		scrollHeight = $(document).height();
		// ドキュメントの高さ
		scrollPosition = $(window).height() + $(window).scrollTop();
		//　ウィンドウの高さ+スクロールした高さ→　現在のトップからの位置
		footHeight = $("footer").innerHeight();
		// フッターの高さ
		if ( scrollHeight - scrollPosition  <= footHeight-344 ) {
			// 現在の下から位置が、フッターの高さの位置にはいったら
			//  "#topcontrol"のpositionをabsoluteに変更し、フッターの高さの位置にする
			$("#topcontrol").css({
				"position":"absolute",
				"bottom": "300px"
			});
		} else {
			// それ以外の場合は元のcssスタイルを指定
			$("#topcontrol").css({
				"position":"fixed",
				"bottom": "100px"
			});
		}
	});

$(".colorbox").colorbox({maxWidth:"80%", maxHeight:"80%"});

// // ヘッダー追従処理
// $(window).load(function(){
// 	if($('body.home').size()){
// 		//TOPの場合
// 		slideHeight = $('#slideArea').height(); //スライドの高さ
// 		headerHeight = $('#header').height(); //ヘッダの高さ
// 		$(window).on('resize', function(){
// 			slideHeight = $('#slideArea').height(); //スライドの高さ
// 			headerHeight = $('#header').height(); //ヘッダの高さ
// 		});
// 		$('#header').clone().prependTo('body').addClass('clone').css({ //メニューをクローンし、bodyの直下に生成。クラス付与 + 初期スタイル付与
// 			'position':'fixed',
// 			'top': 0,
// 			'left': 0,
// 			'margin': 0,
// 			'display':'none',
// 			'background':'#fff',
// 			'box-shadow':'1px 1px 4px #c9c8c8'
// 		});
// 		$(window).on('scroll', function(){
// 			//スクロール時
// 			var locScrl = $(window).scrollTop(); //現在のスクロール値
// 			var fnScrl = slideHeight+(headerHeight / 2);
// 			if(locScrl >= fnScrl) {
// 				//メニューの位置を超えたら
// 				// $('#header.clone').slideDown('fast');
// 				$('#header.clone').fadeIn(500);
// 			} else {
// 				//メニューの位置より上
// 				// $('#header.clone').slideUp('fast');
// 				$('#header.clone').fadeOut(250);
// 			}
// 		});
// 	} else {
// 		//TOP以外
// 		headerHeight = $('#header').height(); //ヘッダの高さ
// 		$('#header').clone().prependTo('body').addClass('clone').css({ //メニューをクローンし、bodyの直下に生成。クラス付与 + 初期スタイル付与
// 			'position':'fixed',
// 			'top': 0,
// 			'left': 0,
// 			'margin': 0,
// 			'display':'none',
// 			'background':'#fff'
// 		});
// 		$(window).on('scroll', function(){
// 			//スクロール時
// 			var locScrl = $(window).scrollTop(); //現在のスクロール値
// 			var fnScrl = headerHeight;
// 			if(locScrl >= fnScrl) {
// 				//メニューの位置を超えたら
// 				// $('#header.clone').slideDown('fast');
// 				$('#header.clone').fadeIn(500);
// 			} else {
// 				//メニューの位置より上
// 				// $('#header.clone').slideUp('fast');
// 				$('#header.clone').fadeOut(250);
// 			}
// 		});
// 	}
// });

});
