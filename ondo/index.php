<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="contents">
<section class="linupArea">
	<div class="inner">
		<h2 class="headTitle">LINEUP<span class="fo18">製品ラインナップ</span></h2>
		<ul class="listArea clearfix">
			<li><a href="<?php bloginfo('url'); ?>/product/unithouse/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img01.jpg" alt="ユニット建築"><span>工場生産により低コスト・高品質・短工期を実現し、住宅・店舗等多用途に活用できます。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/prefab/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img02.jpg" alt="プレハブ建築"><span>自由設計でありながら、規格部材とマニュアル化された施工手順により、低コストを実現。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/system/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img03.jpg" alt="システム建築"><span>無柱空間最大60ｍを可能にした、低コスト・短工期のシステム建築「yess建築」。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/unithouse-lease/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img04.jpg" alt="ユニットハウスリース"><span>現場事務所などの多様なニーズに、連棟・2階建にも対応可能なオンキンユニットリース。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/prefab-lease/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img05.jpg" alt="プレハブリース"><span>ユニットハウスでは対応できない自由な空間設計をプレハブハウスリースが実現します。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/machine-lease/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img06.jpg" alt="建設機械 リース・販売"><span>グラウト機器のレンタル・校正を中心に、各種建設機械をトータルにサポートいたします。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/ashiba/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img07.jpg" alt="仮設足場工事"><span>枠組足場・クサビ式足場・吊り足場など、豊富な施工実績で現場のニーズに対応いたします。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/doboku/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img08.jpg" alt="建設土木用関連商品"><span>各種安全用品、保安用品、工事用品、建築資材をレンタル・販売にて提供いたします。</span></a></li>
			<li><a href="<?php bloginfo('url'); ?>/product/ippan/"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img09.jpg" alt="一般建築・各種工事"><span>一般建築工事から修繕・改築・補強工事まで、確かな技術で幅広いニーズにお応えします。</span></a></li>
		</ul>
	</div>
</section>

<div class="bgSw">
	<section class="aboutArea">
		<div class="inner">
			<h2 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/top/about_title.png" alt="TOTAL BULDING 音頭金属のトータルビルディング" class="pcon"><span class="spon">TOTAL BULDING<span class="spTit">音頭金属のトータルビルディング</span></span></h2>
			<p class="txt"><img src="<?php bloginfo('template_url'); ?>/images/top/about_txt.png" alt="'ユニットハウス・プレハブハウスのレンタル'、'建築工事一式'、'足場工事'の3事業分野を中心に、生産・販売・物流の総合的なネットワークのもと、お客様に高付加価値の製品とサービスを提供させていただいております。" class="pcon"><span class="spon">""ユニットハウス・プレハブハウスのレンタル"、"建築工事一式"、"足場工事"の3事業分野を中心に、<br>
生産・販売・物流の総合的なネットワークのもと、お客様に高付加価値の製品とサービスを提供させていただいております。</span></p>
			<p class="btn"><a href="<?php bloginfo('url'); ?>/product/"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_about.png" alt="詳しくはこちら"></a></p>
		</div>
	</section>
</div>

<section class="workArea">
	<div class="inner">
		<h2 class="headTitle">WORKS<span class="fo18">施工事例</span></h2>
		<ul class="workList clearfix">
			<?php 
			query_posts(
				array(
				'post_type' => 'works',
				'posts_per_page' => 8
				 ) 
			);
			if (have_posts()) : while (have_posts()) : the_post(); 
				$image_id = SCF::get('img_thumb');
				$image = wp_get_attachment_image_src($image_id, 'full');
				if (!empty($image_id)) {
					$img_thumb =  '<img src="'.$image[0].'">';
				};
				$txt_date =  scf::get('works_point');
				?>
			<li><a href="<?php echo get_the_permalink(); ?>"><?php echo $img_thumb; ?><p><span><?php the_title(); ?></span><?php echo $txt_date; ?></p></a></li>
			<?php endwhile; endif; wp_reset_query();?>
		</ul>
		<p class="btn"><a href="<?php bloginfo('url'); ?>/works/"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_work.gif" alt="施工事例をもっと見る"></a></p>
	</div>
</section>

<section class="infoArea">
	<div class="inner clearfix">
		<div class="newArea">
			<h2 class="headTitle tL pb00"><em class="spon">NEWS</em><span class="fo18">新着情報</span></h2>
			<ul class="fo14">
			<?php 
			query_posts(
				array(
				'post_type' => 'news',
				'posts_per_page' => 4
				 ) 
			);
			if (have_posts()) : while (have_posts()) : the_post();	?>
				<li><p class="dates"><?php the_time('Y.m.d'); ?><?php $terms = get_the_terms( get_the_ID(), 'news_cat' );
																			if ( !empty($terms) ) {
																				$term_slug = '';
																				if ( !is_wp_error( $terms ) ) {
																					foreach( $terms as $term ) {
																						$term_slug = $term -> slug;
																						if ($term_slug == 'information'){ $terms_class = 'bgIn';}
																						if ($term_slug == 'recruit'){ $terms_class = 'bgRu';}
																						if ($term_slug == 'works'){ $terms_class = 'bgPr';}
																						echo '<span class="ico '.$terms_class.'">';
																						echo $term->name.'</span>';
																					}
																				}
																			} ?></p>
					<p><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></p>
				</li>
			<?php endwhile; endif; wp_reset_query();?>
			</ul>
		</div>
		<ul class="btnArea">
			<li><a href="<?php bloginfo('url'); ?>/recruit/"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_recruite.jpg" alt="採用情報 募集要項・エントリーはこちら"></a></li>
			<li><span class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_tel.jpg" alt="お電話でのお問い合わせ TEL.076-252-6155 (平日 00:00～00:00)"></span><a href="#" class="spon"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_tel.jpg" alt="お電話でのお問い合わせ TEL.076-252-6155 (平日 00:00～00:00)"></a></li>
			<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_contact.jpg" alt="お問い合わせフォームはこちら"></a></li>
		</ul>
	</div>
</section>

<section class="groupArea">
	<div class="inner">
		<h2 class="ttl"><img src="<?php bloginfo('template_url'); ?>/images/top/group_title.png" alt="GROUP NETWORK 音頭グループネットワーク" class="pcon"><span class="spon">GROUP NETWORK<span class="spTit">音頭グループネットワーク</span></span></h2>
		<p class="txt"><img src="<?php bloginfo('template_url'); ?>/images/top/group_txt.png" alt="音頭グループは、地域に密着した企業活動を通じて、それぞれの地域で最新の技術、最適の製品、最高のサービスをお客さまに提供し、地域社会の発展に貢献していきます。" class="pcon"><span class="spon">音頭グループは、地域に密着した企業活動を通じて、それぞれの地域で最新の技術、最適の製品、最高のサービスをお客さまに提供し、地域社会の発展に貢献していきます。</span></p>
		<p class="btn"><a href="<?php bloginfo('url'); ?>/outline/group/"><img src="<?php bloginfo('template_url'); ?>/images/top/btn_detail.png" alt="詳しくはこちら"></a></p>
	</div>
</section>

<div class="linksArea">
	<div class="inner">
		<ul class="clearfix">
			<li><a href="http://www.onkin.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/top/links_btn01.jpg" alt="音金機械株式会社"></a></li>
			<li><a href="http://www.nihonkaidenka.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/top/links_btn02.jpg" alt="日本海電化鋳造株式会社"></a></li>
			<li><a href="http://www.yamatokankyo.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/top/links_btn03.jpg" alt="大和環境分析センター"></a></li>
			<li><a href="http://toyoseitetsu.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/top/links_btn04.jpg" alt="東洋製鉄株式会社"></a></li>
			<li><a href="http://www.totetsuken.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/top/links_btn05.jpg" alt="東洋建設"></a></li>
			<li><a href="http://www.ondo-metal.co.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/top/links_btn06.jpg" alt="音頭金属株式会社"></a></li>
		</ul>
	</div>
</div>

</div><!-- //#content -->
<?php get_footer(); ?>