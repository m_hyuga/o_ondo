<aside class="subArea">
<div class="snavi">
	<?php if (is_page('outline') || is_parent_slug() === 'outline' ): ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/outline/"><span>会社概要</span></a></p>
	<ul class="fo14">
		<?php wp_nav_menu( array('theme_location'=>'side_outline','container' => '','items_wrap' => '%3$s'));	?>
	</ul>
	<?php endif; if (is_page('product') || is_parent_slug() === 'product' ): ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/product/"><span>製品案内</span></a></p>
	<ul class="fo14">
		<?php wp_nav_menu( array('theme_location'=>'side_product','container' => '','items_wrap' => '%3$s'));	?>
	</ul>
	<?php endif; if (is_page('recruit')): ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/recruit/"><span>採用情報</span></a></p>
	<?php endif; if (is_page('form')): ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/form/"><span>お問い合わせ</span></a></p>
	<?php endif; if (is_page('sitemap')): ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/sitemap/"><span>サイトマップ</span></a></p>
	<?php endif; if (is_post_type_archive('works') || is_singular('works') || is_tax('works_cat')): ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/works/"><span>施工事例</span></a></p>
	<?php
	// カスタム分類名
	$taxonomy = 'works_cat';
	// パラメータ 
	$args = array(
			// 投稿記事がないタームも取得
			'hide_empty' => false
	);
	
	// カスタム分類のタームのリストを取得
	$terms = get_terms( $taxonomy , $args );
	
	if ( count( $terms ) != 0 ) {
			echo '<ul class="fo14">';
			 
			// タームのリスト $terms を $term に格納してループ
			foreach ( $terms as $term ) {
			
					// タームのURLを取得
					$term = sanitize_term( $term, $taxonomy );
					$term_link = get_term_link( $term, $taxonomy );
					if ( is_wp_error( $term_link ) ) {
							continue;
					}
					echo '<li>';
					// タームのURLと名称を出力
					echo '<a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
					echo '</li>';
			}
	echo '</ul>';
	}
	?>
	<?php endif; if (is_post_type_archive('news') || is_singular('news') || is_tax('news_cat')): 
 ?>
	<p class="slinks fo16"><a href="<?php bloginfo('url'); ?>/news/"><span>新着情報</span></a></p>
	<?php
	// カスタム分類名
	$taxonomy = 'news_cat';
	// パラメータ 
	$args = array(
			// 投稿記事がないタームも取得
			'hide_empty' => false
	);
	
	// カスタム分類のタームのリストを取得
	$terms = get_terms( $taxonomy , $args );
	
	if ( count( $terms ) != 0 ) {
			echo '<ul class="fo14">';
			 
			// タームのリスト $terms を $term に格納してループ
			foreach ( $terms as $term ) {
			
					// タームのURLを取得
					$term = sanitize_term( $term, $taxonomy );
					$term_link = get_term_link( $term, $taxonomy );
					if ( is_wp_error( $term_link ) ) {
							continue;
					}
					echo '<li>';
					// タームのURLと名称を出力
					echo '<a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
					echo '</li>';
			}
	echo '</ul>';
	}
	?>
	</div>
	<div class="snavi">
	<p class="slinks fo16"><a ><span>アーカイブ</span></a></p>
	<ul class="fo14">
		<?php wp_get_archives('type=yearly&post_type=news&format=html'); ?>
	</ul>
	<?php endif; ?>

</div>
<p class="pb10"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_tel.jpg" alt="お電話でのお問い合わせ TEL.076-252-6155 (平日 00:00～00:00)"></p>
<p><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_btn.jpg" alt="お問い合わせはこちら"></a></p>
	<?php if (is_post_type_archive('news') || is_singular('news') || is_post_type_archive('works') || is_singular('works')): ?>
	<ul class="subBanner">
		<li><a href="<?php bloginfo('template_url'); ?>/product/unithouse/index.html"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_ban01.jpg" alt="ユニット建築"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_ban02.jpg" alt="ユニットハウスリース"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_ban03.jpg" alt="プレハブ建築"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_ban04.jpg" alt="プレハブハウスリース"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_ban05.jpg" alt="システム建築"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/common/images/sub_ban06.jpg" alt="建設機械 リース・販売"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img07.jpg" alt="仮設足場工事"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img08.jpg" alt="建設土木用 関連商品"></a></li>
		<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/top/lineup_img09.jpg" alt="一般建築・各種工事"></a></li>
	</ul>
	<?php endif; ?>

</aside>