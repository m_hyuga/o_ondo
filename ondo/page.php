<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();

if (is_parent_slug() === 'product' ):
get_template_part( 'page_products' );

else:
?>
<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody" class="<?php if(is_page('greeting')){echo 'ol_greeting';} ?>">
		<?php get_sidebar(); ?>

		<section class="mainArea">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</section>
	</div>
</div><!-- //#content -->

<?php
endif;
get_footer(); ?>
