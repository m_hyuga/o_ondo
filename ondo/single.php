<?php
get_header();
$page_title = esc_html(get_post_type_object(get_post_type())->label);
?>
<script defer src="<?php bloginfo('template_url'); ?>/common/js/jquery.flexslider.js"></script>
<script type="text/javascript">
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
		});
    });
</script>

<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody">
		<?php get_sidebar(); ?>

		<section class="mainArea">
		<?php if (is_singular('works')): ?>
		<?php if (have_posts()) : 
				while (have_posts()) : the_post(); ?>
				<h2 class="headTitle02 line clearfix"><span class="fo24"><?php the_title(); ?></span>
				<?php $terms = get_the_terms( get_the_ID(), 'works_cat' );
				if ( !empty($terms) ) {
					$term_slug = '';
					if ( !is_wp_error( $terms ) ) {
						foreach( $terms as $term ) {
							echo '<span class="smTxt">';
							echo $term->name.'</span>';
						}
					}
				} ?></h2>
				
					
					<?php
						// スライド
						$repeat_group = scf::get('field_imagelist');
						echo '<div class="flexslider">';
						echo '<ul class="slides">';
						foreach ( $repeat_group as $field_name => $field_value ) :
						$val =  $field_value["field_image"];
							if (!empty($val)) {
							$image = wp_get_attachment_image_src($val, 'full');
							echo '<li data-thumb="'.$image[0].'"><img src="'.$image[0].'"></li>';
							}
					 endforeach;
						echo '</ul>';
						echo '</div>';
					 

					 echo '<div class="worksArea">';
					 
					
					
					
					// 施工ポイント
					$works_point = nl2br(get_post_meta($post->ID, 'works_point', true));
					$works_point_d = nl2br(get_post_meta($post->ID, 'works_point_d', true));
					if (!empty($works_point) or !empty($works_point_d)) {
						echo '<div class="txtArea"><div>';
						echo '<p class="title fo20">'.$works_point.'</p>';
						echo '<p class="txt fo14">'.$works_point_d.'</p>';
						echo '<p class="img"><img src="'.get_bloginfo('template_url').'/images/works/img01.png" alt="施工 ポイント"></p>';
						echo '</div></div>';
					};
					 
					 
					 // 詳細情報
					 $works_kouhou = nl2br(get_post_meta($post->ID, 'works_kouhou', true));
					 $works_type = nl2br(get_post_meta($post->ID, 'works_type', true));
					 $works_area = nl2br(get_post_meta($post->ID, 'works_area', true));
					 $works_comp = nl2br(get_post_meta($post->ID, 'works_comp', true));
					 $works_sekou = nl2br(get_post_meta($post->ID, 'works_sekou', true));
					 $works_all = nl2br(get_post_meta($post->ID, 'works_all', true));
					 $works_scale = nl2br(get_post_meta($post->ID, 'works_scale', true));
					 $works_roof = nl2br(get_post_meta($post->ID, 'works_roof', true));
					 $works_wall = nl2br(get_post_meta($post->ID, 'works_wall', true));
					 $works_found = nl2br(get_post_meta($post->ID, 'works_found', true));
					 $works_other = nl2br(get_post_meta($post->ID, 'works_other', true));
					 $works_contact = nl2br(get_post_meta($post->ID, 'works_contact', true));
					 if (!empty($works_kouhou) or !empty($works_type) or !empty($works_area) or !empty($works_comp) or !empty($works_sekou) or !empty($works_all) or !empty($works_scale) or !empty($works_roof) or !empty($works_wall) or !empty($works_found) or !empty($works_other) or !empty($works_contact)) {
						 echo '<h3 class="headTitle03 fo18">詳細情報</h3>';
						 echo '<table class="fo14">';
						 if (!empty($works_kouhou)){ //工法
							 echo '<tr><th>工　　　法</th>';
							 echo '<td>'.$works_kouhou.'</td></tr>';
							}
						 if (!empty($works_type)){ //製品タイプ
							 echo '<tr><th>製品タイプ</th>';
							 echo '<td>'.$works_type.'</td></tr>';
							}
						 if (!empty($works_area)){ //施工地
							 echo '<tr><th>施　工　地</th>';
							 echo '<td>'.$works_area.'</td></tr>';
							}
						 if (!empty($works_comp)){ //完成日
							 echo '<tr><th>完　成　日</th>';
							 echo '<td>'.$works_comp.'</td></tr>';
							}
						 if (!empty($works_sekou)){ //施工日
							 echo '<tr><th>施　工　日</th>';
							 echo '<td>'.$works_sekou.'</td></tr>';
							}
						 if (!empty($works_all)){ //全体工期
							 echo '<tr><th>全 体 工 期</th>';
							 echo '<td>'.$works_all.'</td></tr>';
							}
						 if (!empty($works_scale)){ //施行規模
							 echo '<tr><th>施 工 規 模</th>';
							 echo '<td>'.$works_scale.'</td></tr>';
							}
						 if (!empty($works_roof)){ //屋根
							 echo '<tr><th>屋　　　根</th>';
							 echo '<td>'.$works_roof.'</td></tr>';
							}
						 if (!empty($works_wall)){ //外壁
							 echo '<tr><th>外　　　壁</th>';
							 echo '<td>'.$works_wall.'</td></tr>';
							}
						 if (!empty($works_found)){ //基礎
							 echo '<tr><th>基　　　礎</th>';
							 echo '<td>'.$works_found.'</td></tr>';
							}
						 if (!empty($works_other)){ //その他
							 echo '<tr><th>そ　の　他</th>';
							 echo '<td>'.$works_other.'</td></tr>';
							}
						 if (!empty($works_contact)){ //お問合わせ番号
							 echo '<tr><th>お問合わせ<br class="spon">番号</th>';
							 echo '<td>'.$works_contact.'</td></tr>';
							}
						 echo '</table>';
					 }
					 ?>
					
					<p class="txt fo13 pcon">※工期は原則として基礎着工から完成までです（地盤改良や外構工事は含んでいません）。</p>
				</div>
				<ul class="btnArea">
					<li class="btn">
					<?php $next_post = get_next_post(); 
					$prev_poxt = get_previous_post(); 
					if (!empty( $next_post )){ ?>
						<a href="<?php echo get_permalink( $next_post->ID );  ?>"><img src="<?php bloginfo('template_url'); ?>/images/works/btn_prev.gif" alt="前へ" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/works/sp_btn_prev.gif" alt="前へ" class="spon"></a>
					<?php }else{ echo '&nbsp;';}; ?></li>
					<li class="more"><a href="<?php bloginfo('url'); ?>/works/"><img src="<?php bloginfo('template_url'); ?>/images/works/btn_more.gif" alt="一覧を見る" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/works/sp_btn_more.gif" alt="一覧を見る" class="spon"></a></li>
					<li class="btn">
					<?php if (!empty( $prev_poxt  )){ ?>
					<a href="<?php echo get_permalink( $prev_poxt->ID ); ?>"><img src="<?php bloginfo('template_url'); ?>/images/works/btn_next.gif" alt="次へ" class="pcon"><img src="<?php bloginfo('template_url'); ?>/images/works/sp_btn_next.gif" alt="次へ" class="spon"></a>
					<?php }else{ echo '&nbsp;';}; ?></li>
				</ul>
				<?php endwhile; endif; ?>
				<?php related_posts(); ?>
				
		<?php endif;if (is_singular('news')): ?>
		<?php if (have_posts()) : 
				while (have_posts()) : the_post(); ?>
				<h2 class="headTitle02"><span class="fo24">新着情報</span></h2>
				<div class="newsArea">
					<ul>
						<li>
							<div class="arc_head cf"><span class="time"><?php the_time('Y.m.d'); ?></span><?php $terms = get_the_terms( get_the_ID(), 'news_cat' );
																			if ( !empty($terms) ) {
																				$term_slug = '';
																				if ( !is_wp_error( $terms ) ) {
																					foreach( $terms as $term ) {
																						$term_slug = $term -> slug;
																						if ($term_slug == 'information'){ $terms_class = 'cate_bg02';}
																						if ($term_slug == 'recruit'){ $terms_class = 'cate_bg03';}
																						if ($term_slug == 'works'){ $terms_class = 'cate_bg01';}
																						echo '<span class="cate '.$terms_class.'">';
																						echo $term->name.'</span>';
																					}
																				}
																			} ?></div>
							<div class="cf">
								<h3><?php the_title(); ?></h3>
								<div class="mceContentBody"><?php the_content(); ?></div>
							</div>
						</li>
					</ul>
				</div>
				<?php endwhile; endif; ?>
		<?php endif;?>
		</section>
	</div>
</div><!-- //#content -->
<?php get_footer(); ?>
