<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div id="contents">
	<?php	get_template_part( 'content_pan' ); ?>

	<div id="mainBody">
		<?php get_sidebar(); ?>
		<section class="mainArea">
		
				<h2 class="headTitle02"><span class="fo24">検索結果　<?php $allsearch =& new WP_Query("s=$s&posts_per_page=-1");
	$key = wp_specialchars($s, 1);
	$count = $allsearch->post_count;
	if($count!=0){
	// 検索結果を表示:該当記事あり
			echo '"'.$key.'"　'.$count.'件見つかりました。</p>';
	}
	else {
	// 検索結果を表示:該当記事なし
			echo '"'.$key.'"関連する記事は見つかりませんでした</p>';
	}
	?></span></h2>
				<div class="newsArea">
					<ul>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<li>
							<div class="cf">
								<h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<p><?php
										$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 150);
										$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
										$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
										echo $theContentForPreSingle.'...';
										?></p>
							</div>
						</li>
					<?php endwhile; endif; wp_reset_query();?>


					</ul>
				</div>
				<!-- ページャー -->
				<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</section>
	</div>
</div><!-- //#content -->


<?php get_footer(); ?>
